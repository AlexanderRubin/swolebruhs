import { Link, NavLink } from 'react-router-dom'
import { useGetUserTokenQuery, useLogoutMutation } from '../apiSlice'
import '../css/styles.css'

const Nav = () => {
    const { data: account, isLoading } = useGetUserTokenQuery()
    const [logout] = useLogoutMutation()


    const handleLogout = () => {
        logout()
    }

    if (isLoading) return <div>Loading...</div>

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">
                    SwoleDawgs
                </Link>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav me-auto">
                        {account && (
                            <>
                                <li className="nav-item">
                                    <NavLink
                                        className="nav-link"
                                        to="/workouts/create"
                                    >
                                        Create Workout
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink
                                        className="nav-link"
                                        to="/workouts"
                                    >
                                        Workout List
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink
                                        className="nav-link"
                                        to="/calendar-view"
                                    >
                                        Calendar
                                    </NavLink>
                                </li>
                            </>
                        )}
                    </ul>
                    <ul className="navbar-nav">
                        {!account && (
                            <div className="d-flex">
                                <li className="nav-item mx-2">
                                    {' '}
                                    {/* Align to the right */}
                                    <NavLink to="/login">
                                        <button className="btn btn-primary custom-btn">
                                            Login
                                        </button>
                                    </NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink to="/register">
                                        <button className="btn btn-primary custom-btn">
                                            Register
                                        </button>
                                    </NavLink>
                                </li>
                            </div>
                        )}
                        {account && (
                            <>
                                <div className="d-flex me-auto">
                                    <li className="nav-item mx-2">
                                        <h6
                                            className="nav-link disabled"
                                            aria-disabled="true"
                                        >
                                            <strong>
                                                Welcome,{' '}
                                                {account.account.full_name}!
                                            </strong>
                                        </h6>
                                    </li>
                                    <li className="nav-item">
                                        <button
                                            className="btn btn-outline-danger"
                                            onClick={handleLogout}
                                        >
                                            Logout
                                        </button>
                                    </li>
                                </div>
                            </>
                        )}
                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default Nav
