import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    useGetWorkoutQuery,
    usePostWorkoutSliceMutation,
    useGetUserTokenQuery,
} from '../apiSlice'
import '../css/WorkoutDetails.css' // Import your CSS file

const WorkoutDetails = () => {
    const params = useParams()
    const { data, isLoading } = useGetWorkoutQuery(params.id)
    const [notes, setNotes] = useState('')
    const [completeWorkout] = usePostWorkoutSliceMutation()
    const navigate = useNavigate()
    const { data: account, isLoading: accountLoading } = useGetUserTokenQuery()

    useEffect(() => {
        if (!account && !accountLoading) navigate('/')
    }, [account, accountLoading])

    if (isLoading) {
        return <div>Loading...</div>
    }

    const handleNoteTextAreaChange = (e) => {
        setNotes(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()
        try {
            const workoutRes = await completeWorkout({
                ...data,
                notes: notes,
                is_Complete: true,
            })
            if (workoutRes) navigate('/calendar-view')
        } catch (e) {
            console.log('error', e)
        }
    }

    const exercises = data.exercises

    return (
        <>
            <div className="container">
                <h1>{data.name}</h1>
                <div className="accordion" id="accordionExample">
                    {exercises.map((exercise) => {
                        return (
                            <div className="accordion-item" key={exercise.id}>
                                <h2 className="accordion-header">
                                    <button
                                        className="accordion-button"
                                        type="button"
                                        data-bs-toggle="collapse"
                                        data-bs-target={`#${exercise.id}`}
                                        aria-expanded="true"
                                        aria-controls="collapseOne"
                                    >
                                        Exercise Name: {exercise.exercise.name}
                                    </button>
                                </h2>
                                <div
                                    id={exercise.id}
                                    className="accordion-collapse collapse show"
                                    data-bs-parent="#accordionExample"
                                >
                                    <div className="accordion-body">
                                        <strong>description:</strong>
                                        <p>{exercise.exercise.descriptions}</p>
                                        <strong>sets:</strong>
                                        <p>{exercise.sets}</p>
                                        <strong>reps:</strong>
                                        <p>{exercise.reps}</p>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                    <hr />
                    <div className="container-fluid">
                        <div className="form-floating">
                            <textarea
                                onChange={handleNoteTextAreaChange}
                                className="form-control"
                                placeholder="notes"
                                id="floatingTextarea"
                            ></textarea>
                            <label htmlFor="floatingTextarea">
                                <p>Notes for {data.name}</p>
                            </label>
                        </div>
                    </div>
                    <hr />
                    <div className="form-check">
                        <button
                            type="button"
                            className="btn btn-success"
                            onClick={handleSubmit}
                        >
                            I destroyed this workout!
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default WorkoutDetails
