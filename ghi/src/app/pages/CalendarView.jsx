import { useState, useEffect } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import { useGetWorkoutSliceListQuery, useGetUserTokenQuery } from '../apiSlice';
import { useNavigate } from 'react-router-dom';

const CalendarPage = () => {
  const { data, isSuccess } = useGetWorkoutSliceListQuery();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [workouts, setWorkouts] = useState(null);
  const { data: account, isLoading: accountLoading } = useGetUserTokenQuery();
  const navigate = useNavigate()

    useEffect(() => {
        if (!account && !accountLoading) navigate('/') 
    }, [account, accountLoading])

  useEffect(() => {
    if (isSuccess) {
      const currentDate = new Date()
      const formattedDate = formatDate(currentDate)
      const selectedWorkouts = data.filter((workout) => workout.date_complete.split('T')[0] === formattedDate && workout.user_id === Number(account.account.id))
      setWorkouts(selectedWorkouts)
    }
  }, [isSuccess]);

  const formatDate = (date) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`
  };

  const handleDateChange = async (date) => {
    setSelectedDate(date);
    const formattedDate = formatDate(date);
    if (isSuccess && formattedDate) {
      const selectedWorkouts = data.filter((workout) => workout.date_complete.split('T')[0] === formattedDate && workout.user_id === Number(account.account.id))
      setWorkouts(selectedWorkouts);
    }
  };

  return (
  <div className="accordion" id="accordionExample">
    <div className='container-sm my-3'>
    <h1>Workout Calendar</h1>
      <div className="row">
      <div className="col mb-4">
        <Calendar onChange={handleDateChange} value={selectedDate} />
      </div>
    {/* </div> */}
    {/* <div className='container'> */}
    <div className="col">
    {workouts && workouts.map((workout, index) => (
      <div key={workout.workout_slice_id} className="accordion-item">
        <h2 className="accordion-header" id={`heading${index}`}>
          <button
            className="accordion-button"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target={`#collapse${index}`}
            aria-expanded="true"
            aria-controls={`collapse${index}`}
          >
            <div>
            <b>Workout:</b> {workout.workout.name} <br></br><strong>Date: </strong>{selectedDate.toDateString()}
            </div>
          </button>
        </h2>
        <div
          id={`collapse${index}`}
          className="accordion-collapse collapse"
          aria-labelledby={`heading${index}`}
          data-bs-parent="#accordionExample"
        >
          <div className="accordion-body">
            <p>Date Completed: {new Date(workout.date_complete).toDateString()}</p>
            <p>Workout Name: {workout.workout.name}</p>
            <p>Notes: {workout.notes}</p>
            <ul>
              {workout.workout.exercises.map((exercise) => (
                <li key={exercise.id}>{exercise.exercise.name}</li>
              ))}
            </ul>
          </div>
        </div>
        
      </div>
    ))}
    </div>
    </div>
    </div>
  </div>
);
};

export default CalendarPage;
