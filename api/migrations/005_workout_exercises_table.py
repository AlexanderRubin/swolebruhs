steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE workout_exercises (
            
            id SERIAL PRIMARY KEY NOT NULL,
            workout_id INTEGER NOT NULL REFERENCES workout(id),
            exercise_slice_id INTEGER NOT NULL REFERENCES exercise_slice(id)
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE workout_exercises;
        """,
    ],
]
